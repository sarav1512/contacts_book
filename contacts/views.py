import logging
logger = logging.getLogger(__name__)
from rest_framework import generics,status
from contacts_book.helper import Pagination, ResponseHelper
from serializers import ContactsListSerializer, ContactsCreateSerializer, ContactUpdateSerializer
from .models import Contacts
from rest_framework.permissions import IsAuthenticated


class ContactsListView(generics.ListAPIView):
    pagination_class = Pagination
    permission_classes = (IsAuthenticated, )

    def get_serializer_class(self):
        if self.request.method == "GET":
            return ContactsListSerializer
        return ContactsCreateSerializer

    def get_queryset(self):
        queryset = Contacts().get_queryset(self.request,self.kwargs)
        return queryset

    def get(self,request,*args,**kwargs):
        return super(ContactsListView,self).list(self.request, self.kwargs)

    def post(self,request,*args,**kwargs):
        self.serializer = self.get_serializer(data=self.request.data)
        if not self.serializer.is_valid():
            return ResponseHelper.error_response(self.serializer.errors,status.HTTP_400_BAD_REQUEST)
        response = Contacts().create(request)
        return ResponseHelper.response(response)

class ContactsReadUpdateDeleteView(generics.GenericAPIView):
    serializer_class = ContactsListSerializer
    permission_classes = (IsAuthenticated,)

    def get(self,request,*args,**kwargs):
        response = Contacts().read(request,**kwargs)
        return ResponseHelper.response(response)

    def put(self,request, *args, **kwargs):
        self.serializer = ContactUpdateSerializer(data=self.request.data)
        if not self.serializer.is_valid():
            return ResponseHelper.error_response(self.serializer.errors,status.HTTP_400_BAD_REQUEST)
        response = Contacts().update(request,**kwargs)
        return ResponseHelper.response(response)

    def delete(self,request,*args,**kwargs):
        response = Contacts().remove(request,**kwargs)
        return ResponseHelper.response(response)