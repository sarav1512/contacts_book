import logging
logger = logging.getLogger(__name__)
from django.db import models
from uuid import uuid4
from contacts_book.helper import ResponseHelper
from rest_framework import status

class Book(models.Model):

    uuid = models.UUIDField(primary_key=True,default=uuid4)
    name = models.CharField(max_length=255,unique=True)

    def __unicode__(self):
        return self.name

    def create(self,request):
        try:
            book = Book()
            book.name = request.data.get("name")
            book.save()
            return ResponseHelper.model_response(True, status.HTTP_201_CREATED, {"details":{"uuid": book.uuid}})
        except Exception,e:
            logger.exception(e)
            return ResponseHelper.model_response(False, status.HTTP_500_INTERNAL_SERVER_ERROR, {"message":str(e)})


    def get_queryset(self,request, kwargs):
        try:
            queryset = Book.objects.all()
            return queryset
        except Exception,e:
            logger.exception(str(e))
            return []


