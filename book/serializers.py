from rest_framework import serializers
from .models import Book

class BookListSerializer(serializers.ModelSerializer):
   class Meta:
       model = Book
       fields = ('uuid','name')

class BookSerializer(serializers.Serializer):
    name = serializers.CharField(required=True)

    def validate(self, data):
        if Book.objects.filter(name=data.get("name")).exists():
            raise serializers.ValidationError("Book name already exists")
        return data
