from rest_framework import serializers
from .models import Contacts, Book
from book.serializers import BookListSerializer


class ContactsListSerializer(serializers.ModelSerializer):
    book = BookListSerializer(many=False)
    uuid = serializers.CharField(read_only=True)
    class Meta:
       model = Contacts
       fields = ('uuid','name','book','email','phone')

class ContactsCreateSerializer(serializers.Serializer):
    name  = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)
    book  = serializers.CharField(required=True)
    phone = serializers.CharField(required=False)

    def validate(self, data):
        errors = []
        if Contacts.objects.filter(email=data.get("email")).exists():
            errors.append("Email address already exists")
        if not Book.objects.filter(name=data.get("book")).exists():
            errors.append("Address Book doesn't Exist")
        if errors:
            raise serializers.ValidationError(errors)
        return data


class ContactUpdateSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    name  = serializers.CharField(required=True)
    book = serializers.CharField(required=True)
    phone = serializers.CharField(required=False)

    def validate(self, data):
        errors = []
        if Contacts.objects.filter(email=data.get("email")).exists():
            errors.append("Email address already exists")
        if not Book.objects.filter(name=data.get("book")).exists():
            errors.append("Address Book doesn't Exist")
        if errors:
            raise serializers.ValidationError(errors)
        return data
