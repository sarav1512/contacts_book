from django.conf.urls import url
from views import *
urlpatterns = [
    url(r'^$', ContactsListView.as_view()),
    url(r'^/(?P<pk>[^/]+)$',ContactsReadUpdateDeleteView.as_view())
]