from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from django.db.models import Q
import json

class Pagination(PageNumberPagination):
    """
       Pagination List pagination
    """
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000
    default_page_size = 10

class ResponseHelper():

    @staticmethod
    def response(response):
        data = {"details": response.get("details",{}),"message": response.get("message","")}
        status_code = response.get("status_code")
        return Response(data,status_code)

    @staticmethod
    def error_response(error,status_code):
        data = {"message" : str(error),"status_code":status_code}
        return ResponseHelper.response(data)

    @staticmethod
    def model_response(status, status_code, data):
        response = {}
        response["status"]  = status
        response["status_code"] = status_code
        response["message"] = data.get("message","")
        response["details"] = data.get("details",{})
        return response

class Filter():

    OP_LIST = {
        "is": "",
        "is_not": "",
        "contains": "__icontains",
        "not_contains": "__icontains"
    }

    def __init__(self,filters):
        self.rules = json.loads(filters) if filters else []

    def frame_filters(self):
        rules_q = Q()
        for rule in self.rules:
            operator = Filter.OP_LIST.get(rule.get("operator"))
            q = Q(** {rule.get("field") + operator: rule.get("value")})
            if "not" in rule.get("operator"):
                q.negate()
            rules_q.add(q,rule.get("logical_operator",Q.AND))
        return rules_q


