import logging
logger = logging.getLogger(__name__)
from rest_framework import generics,status
from contacts_book.helper import Pagination, ResponseHelper
from .models import Book
from serializers import BookListSerializer,BookSerializer
from rest_framework.permissions import IsAuthenticated
# Create your views here.

class BookListView(generics.ListAPIView):
    pagination_class = Pagination
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.request.method == "GET":
            return BookListSerializer
        return BookSerializer

    def get_queryset(self):
        queryset = Book().get_queryset(self.request,self.kwargs)
        return queryset

    def get(self,request,*args,**kwargs):
        return super(BookListView,self).list(self.request, self.kwargs)

    def post(self,request,*args,**kwargs):
        self.serializer = self.get_serializer(data=self.request.data)
        if not self.serializer.is_valid():
            return ResponseHelper.error_response(self.serializer.errors,status.HTTP_400_BAD_REQUEST)
        response = Book().create(request)
        return ResponseHelper.response(response)



