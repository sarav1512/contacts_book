import logging
logger = logging.getLogger(__name__)
from django.db import models
from book.models import Book
from uuid import uuid4
from rest_framework import status
from contacts_book.helper import ResponseHelper, Filter

# Create your models here.
class Contacts(models.Model):

    uuid    = models.UUIDField(primary_key=True,default=uuid4)
    book    = models.ForeignKey(Book)
    name    = models.CharField(max_length=255)
    email   = models.EmailField(max_length=255,unique=True)
    phone   = models.CharField(max_length=255,null=True)
    active  = models.BooleanField(default=True)
    created_datetime = models.DateTimeField(auto_now_add=True, null=True)
    modified_datetime = models.DateTimeField(auto_now=True, null=True)

    def create(self,request):
        try:
            contact = Contacts()
            contact.name  = request.data.get("name")
            contact.book  = Book.objects.get(name=request.data.get("book"))
            contact.email = request.data.get("email")
            contact.phone = request.data.get("phone")
            contact.save()
            return ResponseHelper.model_response(True, status.HTTP_201_CREATED, {"details": Contacts.get_serialized_data(contact,request)})
        except Exception,e:
            logger.exception(e)
            return ResponseHelper.model_response(False, status.HTTP_500_INTERNAL_SERVER_ERROR, {"message":str(e)})


    def get_queryset(self,request, kwargs):
        try:
            filters  = Filter(request.GET.get("filters",[])).frame_filters()
            queryset = Contacts.objects.filter(filters)
            return queryset
        except Exception,e:
            import traceback
            traceback.print_exc()
            logger.exception(str(e))
            return []


    def read(self,request,**kwargs):
        try:
            contact = Contacts.objects.get(pk=kwargs.get("pk"))
            return ResponseHelper.model_response(True, status.HTTP_200_OK, {"details":Contacts.get_serialized_data(contact,request)})
        except Contacts.DoesNotExist,e:
            logger.exception(str(e))
            return ResponseHelper.model_response(False, status.HTTP_400_BAD_REQUEST, {"message": "Contact does not exist"})
        except Exception,e:
            logger.exception(str(e))
            return ResponseHelper.model_response(False, status.HTTP_500_INTERNAL_SERVER_ERROR, {"message": str(e)})

    def update(self,request,**kwargs):
        try:
            contact = Contacts.objects.get(pk=kwargs.get("pk"))
            contact.name  = request.data.get("name")
            contact.email = request.data.get("email")
            contact.book  = Book.objects.get(name=request.data.get("book"))
            contact.phone = request.data.get("phone","")
            contact.active = request.data.get("active",True)
            contact.save()
            return ResponseHelper.model_response(True, status.HTTP_200_OK, {"details":Contacts.get_serialized_data(contact,request)})
        except Contacts.DoesNotExist,e:
            logger.exception(str(e))
            return ResponseHelper.model_response(False, status.HTTP_400_BAD_REQUEST, {"message": "Contact does not exist"})
        except Exception,e:
            logger.exception(str(e))
            return ResponseHelper.model_response(False, status.HTTP_500_INTERNAL_SERVER_ERROR, {"message": str(e)})

    def remove(self,request, **kwargs):
        try:
            contact = Contacts.objects.get(pk=kwargs.get("pk"))
            contact.delete()
            return ResponseHelper.model_response(True, status.HTTP_204_NO_CONTENT, {})
        except Contacts.DoesNotExist,e:
            logger.exception(str(e))
            return ResponseHelper.model_response(False, status.HTTP_400_BAD_REQUEST, {"message": "Contact does not exist"})
        except Exception,e:
            logger.exception(str(e))
            return ResponseHelper.model_response(False, status.HTTP_500_INTERNAL_SERVER_ERROR, {"message": str(e)})

    @staticmethod
    def get_serialized_data(obj,request):
        from serializers import ContactsListSerializer
        return ContactsListSerializer(obj,context={'request':request}).data

